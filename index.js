const fs = require('fs-extra');
const path = require('path');
const chalk = require('chalk');

const KEY_CHANGE = 'KEY_CONTAINER';
const KEY_REDUCER_CHANCE = 'KEY_REDUCER';

const FILES = ['index.js', 'styles.js', 'actions.js', 'reducer.js', 'sagas.js'];

process.on('uncaughtException', (err) => {
  console.log(chalk.red.bold(err.message));
  process.exit(1);
});

const copyFile = (fileName, newFilePath) => {
  const defaultPath = __dirname + '/src/DefaultFiles';
  return fs.copyFileSync(defaultPath + '/' + fileName, newFilePath);
}

const executeChanges = (fileName, dirPath, name) => {
  const filePath = dirPath + '/' + fileName;
  copyFile(fileName, filePath);
  changeKeyFile(filePath, name);
};

const changeKeyFile = (filePath, newKey) => {
  const data =  fs.readFileSync(filePath, 'utf-8');

  const re = new RegExp(KEY_CHANGE, 'g');
  let newData = data.replace(re, newKey);

  const reReducer = new RegExp(KEY_REDUCER_CHANCE, 'g');
  newData = newData.replace(reReducer, newKey.toUpperCase());

  return fs.writeFileSync(filePath, newData, 'utf-8');
}

const createDefaultFiles = (container, to) => {
  const dirPath = path.resolve() + '/' + container;

  if (!fs.existsSync(dirPath)){
    fs.mkdirSync(dirPath);
  }

  const files = fs.readdirSync(dirPath);

  for (const defaultFile of FILES) {
    if (!hasFile(files, defaultFile)) {
      executeChanges(defaultFile, dirPath, container);
    }
  }

  fs.move(dirPath, to, err => {
    if(err) return console.log(chalk.red.bold(err));;
    console.log(chalk.green.bold('Success'));
  });

  return true;
}

const getArg = (string) => string.slice(string.indexOf('=') + 1);
const hasFile = (list, file) => list.some((item) => item.includes(file));

const createNewContainer = () => {
  let container = '';
  let to = '.';

  process.argv.forEach((env) => {
    if (env.includes('container=')) {
      container = getArg(env);
    }

    if (env.includes('to=')) {
      to = getArg(env);
    }
  });

  if ((container) && (container.length > 0)) {
    to = to + '/' + container;
    return createDefaultFiles(container, to);
  } else {
    console.log(chalk.red.bold('\n Missing container tag \n'));
    process.exit(1);
  }
}

createNewContainer();
