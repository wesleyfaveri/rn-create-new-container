## Node.js: rn-create-new-container
Create a new container for React-Native with a default Template chosen by myself   ♥

## Installation
`npm install --save-dev rn-create-new-container`

## Script
```
"scripts": {
  "create-container": "node ./node_modules/rn-create-new-container"
},
```

## How2Use
There's 2 args:  
- **container:** *Container name;*   
- **to:** *Dir target;*

**Example**   
`npm run create-container container=Test to=./src`
