import { takeEvery, put } from 'redux-saga/effects';

function* fetchKEY_CONTAINER(action) {
  try {

    yield put({ type: 'KEY_REDUCER_SUCCESS' });
  } catch (err) {
    yield put({ type: 'KEY_REDUCER_ERROR', error: err.message });
  }
}

export default [
  takeEvery('KEY_REDUCER', fetchKEY_CONTAINER),
];
