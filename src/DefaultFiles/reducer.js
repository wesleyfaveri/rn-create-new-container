import { Map } from 'immutable';

const initialState = Map({

});

const handleKEY_CONTAINER = (state, action) => {
  return state;
};

const handleKEY_CONTAINERSuccess = (state, action) => {
  return state;
};

const handleKEY_CONTAINERError = (state, action) => {
  return state;
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'KEY_REDUCER':
      return handleKEY_CONTAINER(state, action);
    case 'KEY_REDUCER_SUCCESS':
      return handleKEY_CONTAINERSuccess(state, action);
    case 'KEY_REDUCER_ERROR':
      return handleKEY_CONTAINERError(state, action);

    default:
      return state;
  }
};

export default reducer;
